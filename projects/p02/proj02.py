#!/usr/env python
"""
Dillon Hicks
PHSX 615
Homework 03 
Problem 2 - SVD
"""
from __future__ import with_statement
import sys
import optparse
import math

import numpy as np
from scipy import stats

import matplotlib.pyplot as plt

Params = None
MATRIX_SIZE = (101, 2)
MATRIX_FILES = [('expt%s.dat' % l) for l in  ('a','b','c')]
ORDER_MIN = 0
ORDER_MAX = 11

GRAPH_POINTS = 1000 
GRAPH_XMIN = -0.05
GRAPH_XMAX = 1.05
GRAPH_YMIN = 0.875
GRAPH_YMAX = 1.2

# Define the set of permitted parameters, including the command
# arguments.  The initialization method creates the parser and defines
# the defaults. The parse() method actually parses the arguments one
# the command line. This was done so that the instance of the class
# could be global and thus available to all routines. and then parse
# the arguments to this call according to the specification
class ConfigurationParameters:
    USAGE = "usage: %prog [options]"

    def __init__(self):
        # Create the argument parser and then tell it about the set of
        # legal arguments for this command. The parse() method of this
        # class calls parse_args of the optparse module
        self.p = optparse.OptionParser(usage=self.USAGE)

        # Boring and totally standard verbose and
        # debugging options that should be common to
        # virtually any command
        #
        self.p.add_option("-d", action="store_const", const=1,        
                          dest="debug_level", 
                          help="Turn on diagnostic output at level 1")
        self.p.add_option("-D", action="store", type ="int",    
                          dest="debug_level", 
                          help="Turn on diagnostic output at level DEBUG_LEVEL")
        self.p.add_option("-v", action="store_const", const=1,        
                          dest="verbose_level", 
                          help="Turn on narrative output at level 1")
        self.p.add_option("-V", action="store", type ="int",    
                          dest="verbose_level", 
                      help="Turn on narrative output at level VERBOSE_LEVEL")
        

        # Examples of misc. extra options that may be used.
        #
        self.p.add_option('-f', '--infile', action='store', type='str',
                          dest='infile',
                          help='The matrix INFILE.')
 
        # Now tell the parser about the default values of all the options
        # we just told it about
        self.p.set_defaults(
            debug_level     = 0,          
            verbose_level   = 0,
            infile          = None,
            # processes       = 1,
            # messages        = 250,
            
            )       
        
    def parse(self):
        self.options, self.args = self.p.parse_args()
        self.debug_level     = self.options.debug_level    
        self.verbose_level   = self.options.verbose_level  

        
        if self.options.infile is not None:
            try:
                self.infile = open(self.options.infile)
            except IOError, eargs:
                self.p.print_usage()
        else:
            self.infile = None


            

    # Defining this method defines the string representation of the
    # object when given as an argument to str() or the "print" command
    def __str__(self):
        param_print_str = \
"""Parameters:
  debug_level    : %d
  verbose_level  : %d
  infile         : %s
""" 

        str_output = param_print_str % \
            (self.debug_level, 
             self.verbose_level,
             self.infile.name)

        return str_output



def matrices_from_file(mfile):
    """
    We make some assumptions that the matrix we are reading in
    conforms to the specification in the homework that each line is a
    cell such that the rows are Xi, Yi, Si.  etc.
    """
    matrix = np.ndarray(MATRIX_SIZE)
    xs = []
    ys = []
    ss = []

    for line in mfile:
        x = float(line.strip())
#        print x,y,sigma
        xs.append(x)
        ys.append(x)
        ss.append(x)

    return xs, ys, ss



def polyfit(xs, ys, order):
    return np.polyfit(xs, ys, order)



def graph(xs, ys, ss, fit_solution, fit_min, fit_max, prefix=''):
    linear_space = np.linspace(GRAPH_XMIN, GRAPH_XMAX, GRAPH_POINTS)
    plt.ylim(GRAPH_YMIN, GRAPH_YMAX)

    plt.plot(xs, ys, '.', linear_space, fit_solution(linear_space), '-')

    plt.errorbar(xs, ys, yerr=ss, linestyle='None')
    order = len(fit_solution)
    plt.title("Fitting Order %d" % order)
    plt.xlabel("X Values")
    plt.ylabel("Y Values")

    if prefix:
        plt.savefig("graph_%d.png" % order)
    else:
        plt.savefig("%s_graph_%d.png" % (prefix, order))

    plt.clf()



def gen_reso(min=0, max=10, sig=1):
    sp = 0.08**2
    ss = 0.10**2

    for n in range(min, max+1):
        res = sig*math.sqrt(sp + n * ss)
        
        yield ((n - res), (n + res))


def in_bin(bin, x):
    return bin[0] <= x <= bin[1]


def bin_data(xs):
    resos = [i for i in gen_reso()]
    resos2 = [i for i in gen_reso(sig=2)]
    resos3 = [i for i in gen_reso(sig=3)]
    for x in xs:
        bin_map =  map(lambda b: in_bin(b,x), resos)
        bin2_map = map(lambda b: in_bin(b,x), resos2)
        bin3_map = map(lambda b: in_bin(b,x), resos3)

        print 1, any(bin_map), 2, any(bin2_map), 3, any(bin3_map), x,
        print filter(lambda b: b, bin_map), 
        print filter(lambda b: b, bin2_map),
        print filter(lambda b: b, bin3_map)


def project_02(mfile):
    xs, ys, ss = matrices_from_file(mfile)
    print max(xs), min(xs)
    bin_data(xs)
    return 
    data_points = zip(xs, ys)
    fit_min = min(data_points)
    fit_max = max(data_points)

    mean = np.mean(ys)
    variance = np.var(ys)
    print mean
    print variance
    print len(ys), len(set(ys))



    for order in range(ORDER_MIN, ORDER_MAX+1):
        raw_fit = polyfit(xs, ys, order)
#        print p
        fit_solution = np.poly1d(raw_fit)

        degrees_of_freedom = len(xs) - 1
        expected_values = [fit_solution(x) for x in xs]
        
        chi2 = sum([ ((o - e)**2)/e for o,e in zip(ys, expected_values)])

        print chi2, chi2 / degrees_of_freedom

        
        graph(xs, ys, ss, fit_solution, fit_min, fit_max, path.splitext(mfile.name))

        


if __name__ == "__main__":
    Params = ConfigurationParameters()
    Params.parse()
    if Params.infile:
        project_02(Params.infile)

    else:
        for mfile in (open(fi) for fi in MATRIX_FILES):
            print mfile.name

            project_02(mfile)
            mfile.close()
            break
