import math

sp = 0.08**2
ss = 0.10**2



def resolution(n):
    return  sp + n * ss


if __name__ == '__main__':
    res_sqrd = [resolution(x) for x in range(0, 20)]
        
    res  = [math.sqrt(i) for i in res_sqrd]
    
    for i, r in enumerate(res):
        print float(i), '+/-', r
