Problem 1:
Epsilon determined to be: 2.22044604925e-16

Problem 2:
value: 0.199405851683, slices: 10^1, error: 16.110394 <= 0.010000... no.
value: 0.169043142431, slices: 10^2, error: 1.569258 <= 0.010000... no.
value: 0.171468072156, slices: 10^3, error: 0.157266 <= 0.010000... no.
value: 0.171765174121, slices: 10^4, error: 0.015731 <= 0.010000... no.
value: 0.171740859874, slices: 10^5, error: 0.001573 <= 0.010000... YES!

Problem 3:
amatrix:
