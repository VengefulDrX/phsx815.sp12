#!/usr/bin/env python
"""
Dillon Hicks
PHSX 615
Homework 1
06.02.12
"""
from __future__ import with_statement
from sys import stdout
import math

#
# Helper functions and constants.
#

def integrate_function(lower_bound=0, upper_bound=1, slices=10**7):
    """

    Integrate the INTEGRAND function from lower_bound to upper_bound
    using the specified number of slices.
    """
    x = 0
    integral_value = 0.0
    slice_width = (float)(upper_bound - lower_bound)/slices
    
    while x < 1:
        integral_value += INTEGRAND(x) * slice_width
        x += slice_width

    return integral_value



def prettify_matrix(matrix):
    """
    Just make the matrix look all pretty.
    """
    retval = ''
    for row in matrix:
        retval += '|'.join(
            ["{:^6}".format(str(elem)) for elem in row])
        retval += '\n'

    return retval



def matrix_from_file(filename):
    """
    Construct a matrix (as list of list of values) from a file. Space
    delimited columns, newline delimited rows.
    """
    matrix = []
    with open(filename) as infile:
        for line in infile:
            matrix.append(
                [int(x) for x in line.split()])
    return matrix

#
# Functions for the problems
#

def problem_1():
    """
    Find the value for the python float value that determines the
    machines precision using the totally standard method.
    """
    print "Problem 1:"
    epsilon = float(1)

    while float(1) + epsilon != float(1):
        # While 1.0 != epsilon+1.0
        last_epsilon = epsilon
        epsilon = last_epsilon/float(2)

    print "Epsilon determined to be:", last_epsilon
    print
    return last_epsilon



def problem_2():
    """
    Integrate the given problem from 0 to 1 numerically. Increase the
    number of slices as 10^N until the error <= DESIRED_ERROR.
    """
    print "Problem 2:"
    
    calc_error = lambda value: \
        (math.fabs(prob2 - EXPECTED_VALUE)/EXPECTED_VALUE) * 100

    for magnitude in range(1,10):        
        prob2 = integrate_function(slices=10**magnitude)
        error = calc_error(prob2)
        stdout.write(
            "value: %s, slices: 10^%s, error: %f <= %f" 
            % (prob2, magnitude, error, DESIRED_ERROR))

        if error <= DESIRED_ERROR:
            print "... YES!"
            break
        print "... no."


    print 



def problem_3():
    '''
    Mutliply two matricies constructed from the files amatrix.txt and
    bmatrix.txt.    
    '''
    print 'Problem 3:'
    amatrix = matrix_from_file("amatrix.txt")
    bmatrix = matrix_from_file("bmatrix.txt")
    result_matrix = [[],[],[],[]]

    # Function that takes a given i and j and creates a value by
    # multiplying the amatrix[i,j] value with the corresponding
    # bmatrix[j,i] value.
    mult = lambda i, j: \
        amatrix[i][j] * bmatrix[j][i]
    
    print "amatrix:"
    print prettify_matrix(amatrix)
    print
    print "bmatrix:"
    print prettify_matrix(bmatrix)
    print

    # Iterate over the indicies for rows and columns and do the
    # corresponding matrix multiplication and add that as the
    # resultant matricies value.
    for row in range(len(amatrix)):
        for column in range(len(amatrix[row])):
            result_matrix[row].append(mult(row,column))
            
    print "result:"
    print prettify_matrix(result_matrix)



if __name__ == "__main__":
    problem_1()
    problem_2()
    problem_3()
