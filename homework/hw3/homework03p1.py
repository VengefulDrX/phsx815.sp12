#!/usr/bin/env python
"""
Dillon Hicks
PHSX 615
Homework 03 
Problem 1 - Monte Carlo
"""
import numpy.random as random # This uses the MT algorithm.
from pprint import pformat
import math
import matplotlib
matplotlib.use('Agg', warn=False)
import matplotlib.pyplot as plt

# The number of measurements 
MC_MEASUREMENTS = 1 * 10**2 # 100

# The number of points per measurements
MC_POINTS = 1 * 10**3 # 1000

SPHERE_RADIUS = 1.0
SPHERE_CUBE_VOLUME_RATIO = 6.0

# The range of random numbers we wish to observe.
RANDOM_RANGE = (0.0, SPHERE_RADIUS)


STATSFILENAME = "pi_values_stats.out"
HISTOFILENAME = "pi_values_histo.png"

# The seed to the Random Number Generator to give reproducible
# results.
RNG_SEED = 10
# Seed the RNG 
random.seed(RNG_SEED)


# Creates strings of the specified character with a length of 60.
BANNER_BIG = "=" * 60
BANNER_SMALL = "-" * 60


# THE index of certain values obtained from the measure_pi_mc
# function.
#
INDEX_PI, INDEX_XS, INDEX_YS, INDEX_ZS = range(4)



class PearsonCorrelation:    
    """
    The pearson correlation coefficients and related typing
    methods.
    """
    NONE    = 0.09
    SMALL   = 0.30
    MEDIUM  = 0.50
    STRONG  = 1.00


    @staticmethod
    def get_coefficient_type(coeff):
        coeff = math.fabs(coeff)
        if coeff <= PearsonCorrelation.NONE:
            return "none"
        elif coeff <= PearsonCorrelation.SMALL:
            return "small"
        elif coeff <= PearsonCorrelation.MEDIUM:
            return "medium"
        elif coeff <= PearsonCorrelation.STRONG:
            return "strong"

        return "coefficient > 1.0... Strong?"


def next_random(): return random.uniform(*RANDOM_RANGE)
def average(points): return sum(points) / len(points)
def score(x, mean, std): return (x - mean) / std


def rms(points):
    """
    Calculates the root mean square for a list of points.
    This could be done with:
    
         xrms^2 = xavg^2 + xstdev^2 

    but optimization is not needed.
    """

    coefficient = 1.0 / len(points)
    sum_squared = sum([i**2 for i in points])
    rms = math.sqrt(coefficient * sum_squared)

    return rms



def stdev(points):
    """
    Calclates the standard deviation of a list of points.
    """

    mean = average(points)
    squares_of_diff = [(i - mean)**2 for i in points]
    stdev = math.sqrt(sum(squares_of_diff) / len(points))

    return stdev



def correlation_coefficient(xs, xmean, xstdev, ys, ymean, ystdev):
    """
    Algorithm: pearson product (wikipedia.org)
    """

    assert(len(xs) == len(ys))

    # 1/(n-1)
    ratio = 1.0 / (len(xs) - 1)

    prod_of_scores = [
        score(x, xmean, xstdev) * score(y, ymean, ystdev) for x,y in zip(xs,ys)]

    r = ratio * sum(prod_of_scores)

    return r



def measure_pi_mc(max_points=MC_POINTS):
    """
    Use the monte carlo method to calculate the value of pi. 

    Returns: The calcluated value of pi and the lists of x, y, and z
    values.
    """

    points_inside = 0

    xs = []
    ys = []
    zs = []

    for point in range(0, max_points):
        x = next_random()
        y = next_random()
        z = next_random()

        xs.append(x)
        ys.append(y)
        zs.append(z)        

        if math.sqrt(x**2 + y**2 + z**2) <= SPHERE_RADIUS:
            points_inside += 1


    pi = SPHERE_CUBE_VOLUME_RATIO * points_inside / max_points

    return (pi, xs, ys, zs)



def calc_averages(xs, ys, zs):
    """
    Calculate the averages of the lists of values xs, ys, and zs.
    """

    xmean = average(xs)
    ymean = average(ys)
    zmean = average(zs)
    
    return xmean, ymean, zmean



def calc_rmses(xs, ys, zs):
    """
    Calculate the rms values for each list of x's, y's, and z's.
    """

    xrms = rms(xs)
    yrms = rms(ys)
    zrms = rms(zs)
    
    return xrms, yrms, zrms


    
def calc_stdevs(xs, ys, zs):
    """
    Calculate the standard deviations for the lists of x's, y's, and
    z's.
    """

    xstdev = stdev(xs)
    ystdev = stdev(ys)
    zstdev = stdev(zs)
    
    return xstdev, ystdev, zstdev



def calc_coefficients(xs, xmean, xstdev,
                      ys, ymean, ystdev,
                      zs, zmean, zstdev):
    """
    Calculate the correlation coefficients for x vs. y, x vs. z, and y
    vs. z using the pearson method.
    """

    xy_corr = correlation_coefficient(
        xs, xmean, xstdev, ys, ymean, ystdev)
    
    xz_corr = correlation_coefficient(
        xs, xmean, xstdev, zs, zmean, zstdev)

    yz_corr = correlation_coefficient(
        ys, ymean, ystdev, zs, zmean, zstdev)

    
    return (xy_corr, xz_corr, yz_corr)



def create_histogram(values, bins=10, 
                     lbound=None, ubound=None,
                     xlabel="xaxis", ylabel="yaxis",
                     title="title",
                     filename="histo.png"):
    """
    Uses matplot lib to create a histogram from the list of "values" argument.
    """

    if not lbound: lbound = min(values)
    if not ubound: ubound = max(values)

    n, bins, patches = plt.hist(values,
                                bins=bins,
                                range=(lbound, ubound), 
                                histtype='bar')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid(True)

    plt.savefig(filename)

    plt.clf()



def problem_1():
    """
    Calculate the pi via a sphere using the monte carol method.
    """

    measurements = (measure_pi_mc() for m in range (0, MC_MEASUREMENTS))

    all_pis = []

    for i, m in enumerate(measurements, start=1): 
        
        xmean, ymean, zmean = calc_averages(*m[INDEX_XS:])
        xrms, yrms, zrms = calc_rmses(*m[INDEX_XS:])
        xstdev, ystdev, zstdev = calc_stdevs(*m[INDEX_XS:])

        xy_corr, xz_corr, yz_corr = calc_coefficients(
            m[INDEX_XS], xmean, xrms, 
            m[INDEX_YS], ymean, yrms, 
            m[INDEX_ZS], zmean, zrms)

        print BANNER_BIG
        print "Measurement #%d" % i
        print BANNER_SMALL

        print "PI =", m[INDEX_PI]

        print "Means:   X = %f, Y = %f, Z = %f" % (xmean, ymean, zmean) 
        print "STDEV's: X = %f, Y = %f, Z = %f" % (xstdev, ystdev, zstdev) 
        print "RMS's:   X = %f, Y = %f, Z = %f" % (xrms, yrms, zrms) 

        print 

        print "Correlations:"
        print BANNER_SMALL

        print "XY = %f [%s]" % \
            (xy_corr, PearsonCorrelation.get_coefficient_type(xy_corr))
        print "XZ = %f [%s]" % \
            (xz_corr, PearsonCorrelation.get_coefficient_type(xz_corr))
        print "YZ = %f [%s]" % \
            (yz_corr, PearsonCorrelation.get_coefficient_type(yz_corr))

        print BANNER_BIG
    
        all_pis.append(m[INDEX_PI])


    pi = average(all_pis)
    pi_count = len(all_pis)
    pi_stdev = stdev(all_pis)
    pi_var = pi_stdev**2
    pi_rms = rms(all_pis)
    pi_min = min(all_pis)
    pi_max = max(all_pis)

    print
    print "PI (Average) = ", pi
    print

    
    print 'Writing stats for PI calculation...'
    with open(STATSFILENAME, 'w') as outfile:
        print >> outfile, 'Stats for the Monte Carlo PI Calculation'
        print >> outfile, BANNER_BIG
        print >> outfile, 'Mean (PI):', pi
        print >> outfile, 'Count:', pi_count
        print >> outfile, 'Min:', pi_min
        print >> outfile, 'Max:', pi_max
        print >> outfile, 'STDEV:', pi_stdev
        print >> outfile, 'Variance:', pi_var
        print >> outfile, 'RMS:', pi_rms
    print 'Stats written to %s' % STATSFILENAME

    print

    print "creating histogram..."

    create_histogram(
        all_pis, title="Monte Carlo Measurements of PI", 
        ylabel="occurrences",
        xlabel="values of pi",
        filename=HISTOFILENAME)
    

    print "histogram written to %s" % HISTOFILENAME 



if __name__ == "__main__":
    problem_1()
