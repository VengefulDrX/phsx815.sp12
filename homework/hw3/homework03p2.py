#!/usr/bin/env python
"""
Dillon Hicks
PHSX 615
Homework 03 
Problem 2 - SVD
"""
from __future__ import with_statement
import sys
import optparse
import numpy as np

Params = None
MATRIX_SIZE = (101, 2)
MATRIX_FILE = "design_matrix.txt"

# Define the set of permitted parameters, including the command
# arguments.  The initialization method creates the parser and defines
# the defaults. The parse() method actually parses the arguments one
# the command line. This was done so that the instance of the class
# could be global and thus available to all routines. and then parse
# the arguments to this call according to the specification
class ConfigurationParameters:
    USAGE = "usage: %prog [options]"

    def __init__(self):
        # Create the argument parser and then tell it about the set of
        # legal arguments for this command. The parse() method of this
        # class calls parse_args of the optparse module
        self.p = optparse.OptionParser(usage=self.USAGE)

        # Boring and totally standard verbose and
        # debugging options that should be common to
        # virtually any command
        #
        self.p.add_option("-d", action="store_const", const=1,        
                          dest="debug_level", 
                          help="Turn on diagnostic output at level 1")
        self.p.add_option("-D", action="store", type ="int",    
                          dest="debug_level", 
                          help="Turn on diagnostic output at level DEBUG_LEVEL")
        self.p.add_option("-v", action="store_const", const=1,        
                          dest="verbose_level", 
                          help="Turn on narrative output at level 1")
        self.p.add_option("-V", action="store", type ="int",    
                          dest="verbose_level", 
                      help="Turn on narrative output at level VERBOSE_LEVEL")
        

        # Examples of misc. extra options that may be used.
        #
        self.p.add_option('-f', '--infile', action='store', type='str',
                          dest='infile',
                          help='The matrix INFILE.')
 
        # Now tell the parser about the default values of all the options
        # we just told it about
        self.p.set_defaults(
            debug_level     = 0,          
            verbose_level   = 0,
            outfile         = None,
            infile          = MATRIX_FILE,
            # processes       = 1,
            # messages        = 250,
            
            )       
        
    def parse(self):
        self.options, self.args = self.p.parse_args()
        self.debug_level     = self.options.debug_level    
        self.verbose_level   = self.options.verbose_level  

        
        if self.options.infile is not None:
            try:
                self.infile = open(self.options.infile)
            except IOError, eargs:
                self.p.print_usage()

        else:
            self.p.print_usage()
            print 'Error: no input file specified with -f <infile>'


            

    # Defining this method defines the string representation of the
    # object when given as an argument to str() or the "print" command
    def __str__(self):
        param_print_str = \
"""Parameters:
  debug_level    : %d
  verbose_level  : %d
  infile         : %s
""" 

        str_output = param_print_str % \
            (self.debug_level, 
             self.verbose_level,
             self.infile.name)

        return str_output



def matrix_from_file(mfile):
    """
    We make some assumptopns that the matrix we are reading in
    conforms to the specification in the homework that each line is a
    cell such that the rows are A[0][0], A[0][1], A[1][0], A[1][1],
    etc.
    """
    matrix = np.ndarray(MATRIX_SIZE)
    row = 0
    column = 0
    for number, line in enumerate(mfile):
        column = number % 2
        matrix[row][column] = float(line)
        if column == 1:
            row += 1

    return matrix
    

def print_svd_debug_info(U, Vh, s, S):
    """
    Just pretty print all of the matricies as a sanity check.
    """
    banner_line = "=" * 60
    print banner_line
    print " " * 10, "START SINGLE VALUE DECOMP DEBUG INFO"
    print banner_line

    print "U - Matrix"
    print banner_line
    print U
    print
    
    print "Vh - Matrix" 
    print banner_line
    print Vh
    print 
    
    print "s - Single Value Matrix"
    print banner_line
    print s
    print 
    
    print "S - Diagnal Matrix"
    print banner_line
    print S
    print

    print banner_line
    print " " * 10, "END SINGLE VALUE DECOMP DEBUG INFO"
    print banner_line
    print


def problem_2(mfile):
    """
    Computes the SVD of a matrix described in matrix_file and assuming
    the following property holds: 
    
       (1) A == U * S * Vh
        
    Completes a sanity check to make sure the values determined from
    the SVD are consistent.
    """
    # Read in the matrix from the file. 
    matrix =  matrix_from_file(mfile)

    # Note about one of the options to SVD:
    #
    #  full_matrices : boolean, optional
    # If True (default), ``U`` and ``Vh`` are shaped
    # ``(M,M)`` and ``(N,N)``.  Otherwise, the shapes are
    # ``(M,K)`` and ``(K,N)``, where ``K = min(M,N)``.
    U, s, Vh =  np.linalg.svd(matrix, full_matrices=False)

    # Get the diagnal matrix from s so we may compute A (see (1)).
    S = np.diag(s)
    
    if Params.debug_level:
        print_svd_debug_info(U, Vh, s, S)
    
    # Use the np fortran backend to compute:
    #
    # result_matrix = U * S * Vh.
    # 
    # Within a tolerance. (The default is about 1e-5 relative
    # tolerance and 1e-8 absolute toleracne).
    result_matrix = np.dot(U, np.dot(S, Vh))

    matrices_equal = np.allclose(
        matrix, np.dot(U, np.dot(S, Vh)))

    print "Matricies are Equal? (A == U * S * Vh):", matrices_equal
        

    return U, Vh, s, S

if __name__ == "__main__":
    Params = ConfigurationParameters()
    Params.parse()

    problem_2(Params.infile)
